<?php
    if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else {
        $file_name = $_FILES['file']['name'];
        $ext = end((explode(".", $file_name)));
        $target_file_name = "video.$ext";
        $message->upload_file_name = $file_name;
        // move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . $_FILES['file']['name']);
        move_uploaded_file($_FILES['file']['tmp_name'], "uploads/$target_file_name");
        // echo "Success: {$file_name}上傳完成！";

        chdir("/home/nchc/yolact-police");
        shell_exec("rm -rf results/ >> out.log");
        shell_exec("rm -f out.log >> out.log");
        shell_exec("rm -f police.db >> out.log");
        putenv('PATH=/home/nchc/bin:/home/nchc/.local/bin:/usr/local/cuda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin');
        putenv('LD_LIBRARY_PATH=/usr/local/cuda-10.0/lib64');
        // $command = "python3 eval.py --video=/var/www/smart_video_analysis/uploads/{$file_name}";
        $command = "python3 eval.py --video=/var/www/smart_video_analysis/uploads/$target_file_name";
        switch ($_POST['fps']) {
            case '1':
                $command .= ' --fps=1';
                break;
            case '10':
                $command .= ' --fps=10';
                break;
            default:
                # code...
                break;
        }
        // echo $command;
        // $shell_log = shell_exec("/usr/bin/nohup ".$command.' >/dev/null 2>&1 >> out.log &');
        $shell_log = shell_exec($command.' >> out.log');
        $message->log = "Success: 上傳分析完成！";
        echo json_encode($message);

    }
?>
