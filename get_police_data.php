<?php

$folder = '/home/nchc/yolact-police/';
$db_name = 'police.db';

// Get start datetime and end datetime
// $datetime_start = empty($_GET['datetime_start']) ? '': $_GET['datetime_start'] . ':00';
// $datetime_end = empty($_GET['datetime_end']) ? '': $_GET['datetime_end'] . ':00';
// $location = '';

// filter car type and color
$condition = '';
$type_count = 0;
for ($type_num = 1; $type_num <= 5; $type_num++){
    if (!empty($_GET["type$type_num"])) {
        $type_count++;
        $attribute_items = '';
        $arrtibute_count = 0;
        for ($attribute_num = 1; $attribute_num <= 7; $attribute_num++){
            if (!empty($_GET["type$type_num$attribute_num"])){
                $arrtibute_count++;
                if ($arrtibute_count <= 1){
                    $attribute_items .= "'" . $_GET["type$type_num$attribute_num"] . "'";
                }
                else {
                    $attribute_items .= ", '" . $_GET["type$type_num$attribute_num"] . "'";
                }
            }
        }
        // 第一個 sql where 條件
        if ($type_count <= 1){
            $condition .= sprintf("(type IN ('%s') AND attribute IN (%s))", $_GET["type$type_num"], $attribute_items);
        }
        // 第二個以後的 sql where 條件
        else {
            $condition .= sprintf("OR (type IN ('%s') AND attribute IN (%s))", $_GET["type$type_num"], $attribute_items);
        }
    }
}

// filter person clothes
for ($type_num = 6; $type_num <= 9; $type_num++){
    if (!empty($_GET["type$type_num"])){
        $type_count++;
        $attribute_items = array();
        for ($attribute_num = 1; $attribute_num <= 11; $attribute_num++){
            if (!empty($_GET["type$type_num$attribute_num"])){
                array_push($attribute_items, $_GET["type$type_num$attribute_num"] . ' ' . $_GET["type$type_num"]);
            }
        }
        // print_r($attribute_items);
        // 第一個 sql where 條件
        foreach ($attribute_items as $value) {
            if ($type_count <= 1){
                $type_count++;
                $condition .= sprintf("(type IN ('person') AND attribute like '%%%s%%')", $value);
            }
            // 第二個以後的 sql where 條件
            else {
                $condition .= sprintf(" OR (type IN ('person') AND attribute like '%%%s%%')", $value);
            }
        }

    }
}
// print_r($condition);

// for ($type_num = 6; $type_num <= 6; $type_num++){
//     if (!empty($_GET["type$type_num"])) {
//         for ($attribute_num = 1; $attribute_num <= 4; $attribute_num++){
//             if (!empty($_GET["type$type_num$attribute_num"])){
//                 $type_count++;
//                 $attribute_cond = '';
//                 switch ($_GET["type$type_num$attribute_num"]) {
//                     case 'short_sleeves_top':
//                         // short sleeves top, short sleeves outwear, vest, sling = 短袖上衣
//                         $attribute_cond = " attribute LIKE '%short sleeves top%' 
//                                             OR attribute LIKE '%short sleeves outwear%' 
//                                             OR attribute LIKE '%vest%' 
//                                             OR attribute LIKE '%sling%' ";
//                         break;
//                     case 'long_sleeves_top':
//                         // long sleeves top, long sleeves outwear = 長袖上衣
//                         $attribute_cond = " attribute LIKE '%long sleeves top%' 
//                                             OR attribute LIKE '%long sleeves outwear%' ";
//                         break;
//                     case 'shorts':
//                         // shorts = 短褲
//                         $attribute_cond = " attribute LIKE '%shorts%' ";
//                         break;
//                     case 'trousers':
//                         // trousers = 長褲
//                         $attribute_cond = " attribute LIKE '%trousers%' ";
//                         break;
//                     default:
//                         # code...
//                         break;
//                 }
//                 // echo "count: $type_count <br>";

//                 // 第一個 sql where 條件
//                 if ($type_count <= 1){
//                     $condition .= sprintf("(type IN ('%s') AND (%s))", $_GET["type$type_num"], $attribute_cond);
//                 }
//                 // 第二個以後的 sql where 條件
//                 else {
//                     $condition .= sprintf("OR (type IN ('%s') AND (%s))", $_GET["type$type_num"], $attribute_cond);
//                 }

//             }
//         }
//     }
// }

// print_r($_GET);
// echo "<br>";
// print_r($condition);




$db = new SQLite3($folder . $db_name);
// echo $db->lastErrorMsg();
// $sql = sprintf("SELECT image, type, attribute FROM police WHERE (%s)", $condition);
$sql = sprintf("SELECT crop_image, type, attribute, original_image FROM police WHERE (%s)", $condition);
// echo "<br>".$sql;


echo '<div class="container"><div class="row justify-content-center align-items-center">';
echo '<div class="col-12 text-right"><a href="images/image_result.zip" class="btn btn-sm btn-info" role="button">下載檢索結果<i class="fa fa-download" aria-hidden="true"></i></a>  </div>';

$res = $db->query($sql);
$images = '';
if ($res){
    while ($row = $res->fetchArray()){
        // $html = sprintf('<div class="col-sm-6 col-md-3"><img class="w-75" src="images/%s"></div>', $row[0]);
        $html = sprintf('<div class="col-sm-6 col-md-3"><a href="images/original/%s"><img class="w-75" src="images/crop/%s"></a></div>',$row[3] , $row[0]);
        echo $html;
        $images .= $row[0] . ' ';
    }
    $command = "zip /home/nchc/yolact-police/results/image_result.zip " . $images;
    chdir("$folder" . "results/crop");
    // echo getcwd();
    // echo $command;
    $output = shell_exec("$command >> out.log");

}




echo '<div class="col-12 text-right"><a href="images/image_result.zip" class="btn btn-sm btn-info" role="button">下載檢索結果<i class="fa fa-download" aria-hidden="true"></i></a>  </div>';
echo '</div></div>';

// echo "<script type='text/javascript'> console.log(\"$sql\")</script>";


?>