# 
# insert police data to sqlite3 DB
#
# Require:
#   * python 3.5
#   * python 3 sqlite3
#   * datetime format must use "2020-04-02 03:02:01"
#  20200609 V.0.1
#   * del image column
#   * add crop_image, original_image columns
#

from db_api import insert_db

insert_db(db_file="police.db", date='2020-04-02 03:02:01', folder='video1-001', crop_image='image1', original_image='image2', type='type1', attribute='attribute1 | attribute1', x1='x1', y1='y1', x2='x2', y2='y2')
