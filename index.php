<?php 
$color_index = array(
    "black"  => "黑",
    "blue"   => "藍",
    "green"  => "綠",
    "red"    => "紅",
    "silver" => "銀",
    "white"  => "白",
    "yellow" => "黃"
);

function gen_color_checkbox_html($type, $color_index){
    $count = 0;
    foreach ($color_index as $key => $value) {
        $count++;
        echo "<label for='type$type$count' >$value</label>";
        echo "<input name='type$type$count' id='type$type$count' type='checkbox' value='$key'>";
    }
}



$person_color_index = array(
    "black"  => "黑",
    "blue"   => "藍",
    "brown"  => "咖啡",
    "gray"   => "灰",
    "green"  => "綠",
    "orange" => "橘",
    "pink"   => "粉紅",
    "purple" => "紫",
    "red"    => "紅",
    "white"  => "白",
    "yellow" => "黃"
);

function gen_person_color_checkbox_html($type, $person_color_index){
    $count = 0;
    foreach ($person_color_index as $key => $value) {
        $count++;
        echo "<label for='type$type$count' >$value</label>";
        echo "<input name='type$type$count' id='type$type$count' type='checkbox' value='$key'>";
    }
}

function gen_person_color_select_html($type, $person_color_index){
    $count = 0;
    echo "<label for='type{$type}1'>衣著顏色：</label>";
    echo "<select id='type{$type}1' name='type{$type}1' class='custom-select'>";
    foreach ($person_color_index as $key => $value) {
        $count++;
        if ($count == 1){
            echo "    <option value='$key' selected>$value</option>";
        }
        else {
            echo "    <option value='$key'>$value</option>";
        }
        
    }
    echo '</select>';

}


?>
<html lang="zh-Hant-TW">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="asset/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- google css -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet"> -->
    <link href="asset/googleapisnoto/css.css?family=Noto+Sans+TC&display=swap" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <link href="asset/googleapisnunito/css.css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script> -->
    <script src="asset/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <!-- <script src="https://kit.fontawesome.com/e895e59bdb.js" crossorigin="anonymous"></script> -->


    <!-- bootstrap-datetimepicker javascript -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> -->
    <script src="asset/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="asset/ajax/libs/moment.js/2.25.1/moment-with-locales.min.js"></script>
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script> -->
    <script type="text/javascript" src="asset/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" /> -->
    <link rel="stylesheet" href="asset/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />

    <!-- JQuery UI -->
    <!-- <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script> -->
    <!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" /> -->
    <link rel="stylesheet" href="asset/ui/1.12.1/themes/base/jquery-ui.css" />
    <!-- <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css" /> -->
    <link rel="stylesheet" href="asset/resources/demos/style.css" />
    <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
    <script src="asset/ui/1.12.1/jquery-ui.js"></script>
    

    <script>
        window.onload = function (){
            // 清除所有車輛搜尋選項
            $('input:checkbox').removeAttr('checked');
        }

        $( function() {
            $('input[type="checkbox"]').checkboxradio();
        } );

    </script>


    <title>智慧視訊分析與檢索系統</title>
    <style>
        /* Always set the map height explicitly to define the size of the div
        * element that contains the map. */
        #map {
        height: 800px;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        }

        /* check box size*/
        .checkbox-lg .custom-control-label::before, 
        .checkbox-lg .custom-control-label::after {
            top: .8rem;
            width: 1.55rem;
            height: 1.55rem;
        }

        .checkbox-lg .custom-control-label {
            padding-top: 13px;
            padding-left: 6px;
        }


        .checkbox-xl .custom-control-label::before, 
        .checkbox-xl .custom-control-label::after {
            top: 1.2rem;
            width: 1.85rem;
            height: 1.85rem;
        }

        .checkbox-xl .custom-control-label {
            padding-top: 23px;
            padding-left: 10px;
        }
    </style>

</head>

<body>
    <script type="text/javascript">
        $(function(){
            // alert('type1' + $("type1").val());

        });        
    </script>
    
    <div class="container-fluid">
        <div class="">
            <div class="row" >
                <div class="col-12 border">
                <!-- <div class="col-3 border"> -->
                    <!-- <img src="http://cloud.nchc.org.tw/smart_light/img/logo_nchc.jpg" style="width: 100%; object-fit: cover; height: 100%;"> -->
                    <h4 class="h4 mb-0 text-gray-800">智慧視訊分析與檢索系統</h4><br>
                </div>
            </div>
            
            <div class="row">
                <div class="col-1">
                </div>
                <div class="col-3 form-inline">
                    <label for="fps">選擇模式：</label>
                    <select id="fps" name="fps" class="custom-select">
                        <option value="1">簡易模式</option>
                        <option value="10">標準模式</option>
                        <option value="none">深度模式</option>
                    </select>
                </div>
                <div class="col-4 form-inline">
                    <input type="file" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">選擇上傳擋案</label>
                    <script>
                        $('#customFile').on('change',function(){
                            //get the file name
                            var fileName = $(this).val();
                            //replace the "Choose a file" label
                            $(this).next('.custom-file-label').html(fileName);
                        })
                    </script>
                </div>
                <div class="col-4 form-inline">
                    <button id="btn_upload" class="btn-primary">上傳／分析</button>
                </div>
            </div>
            <div class="row">
                <div class="col-12 border-bottom-0">
                    <!-- <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist" style="background-color: #F0F0F0">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">檢索條件</a>
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">檢索結果</a>
                            <a class="nav-item nav-link disabled" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">新功能</a>
                        </div>
                    </nav> -->
                    <!-- <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist" style="background-color: #F0F0F0">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">檢索條件</a>
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">檢索結果</a>
                            <a class="nav-item nav-link disabled" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">新功能</a>
                        </div>
                    </nav> -->
                    <ul class="nav nav-tabs" id="nav-tab" role="tablist" style="background-color: #F0F0F0">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">檢索條件</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">檢索結果</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" id="messages-tab" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="false">新功能</a>
                        </li>

                </div>
            </div>

            
            <div class="row">
                <div class="col-3 border">
                    <div>上傳檔案列表</div>
                    <label id="upload_file_list"></label>
                </div>
                <div class="col-9 border border-top-0">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <form>
                                篩選條件<br>
                                <!-- 時間區間<br>
                                起：<input name="datetime_start" type="text" class="form-control datetimepicker-input" id="datetimepicker5" data-toggle="datetimepicker" data-target="#datetimepicker5"/>
                                迄：<input name="datetime_end"type="text" class="form-control datetimepicker-input" id="datetimepicker_end" data-toggle="datetimepicker" data-target="#datetimepicker_end"/> -->
                                <div>
                                    車輛：<br>
                                    <div class="row">
                                        <div class="col-3">
                                            <input name="type1" id="type1" type="checkbox" value="bicycle"><label for="type1">腳踏車</label>
                                        </div>
                                        <div class="col-9">
                                            <?php
                                            gen_color_checkbox_html('1', $color_index);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3">
                                            <input name="type2" id="type2" type="checkbox" value="motorcycle"><label for="type2">機車&emsp;</label>
                                        </div>
                                        <div class="col-9">
                                            <?php
                                            gen_color_checkbox_html('2', $color_index);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3">
                                            <input name="type3" id="type3" type="checkbox" value="car"><label for="type3">轎車&emsp;</label>
                                        </div>
                                        <div class="col-9">
                                            <?php
                                            gen_color_checkbox_html('3', $color_index);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3">
                                            <input name="type4" id="type4" type="checkbox" value="truck"><label for="type4">貨卡車</label>
                                        </div>
                                        <div class="col-9">
                                            <?php
                                            gen_color_checkbox_html('4', $color_index);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3">
                                            <input name="type5" id="type5" type="checkbox" value="bus"><label for="type5">巴士&emsp;</label>
                                        </div>
                                        <div class="col-9">
                                            <?php
                                            gen_color_checkbox_html('5', $color_index);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3">
                                            <input name="type6" id="type6" type="checkbox" value="short_sleeves"><label for="type6">行人：短袖</label>
                                        </div>
                                        <div class="col-9 form-inline"> <!-- form-inline -->
                                            <?php
                                            // gen_person_color_checkbox_html('6', $person_color_index);
                                            gen_person_color_select_html('6', $person_color_index);
                                            ?>

                                            <!-- <label for="person_item_color">衣著顏色：</label>
                                            <select id="person_item_color" name="person_item_color" class="custom-select">
                                                <option value="type61" selected>黑色</option>
                                                <option value="type62" >藍色</option>
                                                <option value="type63">咖啡色</option>
                                                <option value="type64">灰色</option>
                                                <option value="type65">綠色</option>
                                                <option value="type66">橘色</option>
                                                <option value="type67">粉紅色</option>
                                                <option value="type68">紫色</option>
                                                <option value="type69">紅色</option>
                                                <option value="type610">白色</option>
                                                <option value="type611">黃色</option>
                                            </select> -->
                                            <!-- &emsp;&emsp;&emsp;
                                            <label for="person_item_class">衣著型式：</label>
                                            <select id="person_item_class" name="person_item_class" class="custom-select">
                                                <option value="">短袖</option>
                                                <option value="">長袖</option>
                                                <option value="">短褲</option>
                                                <option value="">長褲</option>
                                            </select> -->

                                            <!-- <label for='type61'>短袖上衣</label>
                                            <input name='type61' id='type61' type='checkbox' value='short_sleeves_top'>
                                            <label for='type62'>長袖上衣</label>
                                            <input name='type62' id='type62' type='checkbox' value='long_sleeves_top'>
                                            <label for='type63'>短褲&emsp;</label>
                                            <input name='type63' id='type63' type='checkbox' value='shorts'>
                                            <label for='type64'>長褲&emsp;</label>
                                            <input name='type64' id='type64' type='checkbox' value='trousers'> -->
                                        </div>



                                        <div class="col-3">
                                            <input name="type7" id="type7" type="checkbox" value="long_sleeves"><label for="type7">行人：長袖</label>
                                        </div>
                                        <div class="col-9 form-inline">
                                            <?php
                                            // gen_person_color_checkbox_html('7', $person_color_index);
                                            gen_person_color_select_html('7', $person_color_index);
                                            ?>
                                            <!-- <label for="person_item_color">衣著顏色：</label>
                                            <select id="person_item_color" name="person_item_color" class="custom-select">
                                                <option value="type71" selected>黑色</option>
                                                <option value="type72" >藍色</option>
                                                <option value="type73">咖啡色</option>
                                                <option value="type74">灰色</option>
                                                <option value="type75">綠色</option>
                                                <option value="type76">橘色</option>
                                                <option value="type77">粉紅色</option>
                                                <option value="type78">紫色</option>
                                                <option value="type79">紅色</option>
                                                <option value="type710">白色</option>
                                                <option value="type711">黃色</option>
                                            </select> -->
                                            <!-- &emsp;&emsp;&emsp;
                                            <label for="person_item_class">衣著型式：</label>
                                            <select id="person_item_class" name="person_item_class" class="custom-select">
                                                <option value="">短袖</option>
                                                <option value="">長袖</option>
                                                <option value="">短褲</option>
                                                <option value="">長褲</option>
                                            </select> -->

                                            <!-- <label for='type61'>短袖上衣</label>
                                            <input name='type61' id='type61' type='checkbox' value='short_sleeves_top'>
                                            <label for='type62'>長袖上衣</label>
                                            <input name='type62' id='type62' type='checkbox' value='long_sleeves_top'>
                                            <label for='type63'>短褲&emsp;</label>
                                            <input name='type63' id='type63' type='checkbox' value='shorts'>
                                            <label for='type64'>長褲&emsp;</label>
                                            <input name='type64' id='type64' type='checkbox' value='trousers'> -->
                                        </div>



                                        <div class="col-3">
                                            <input name="type8" id="type8" type="checkbox" value="shorts"><label for="type8">行人：短褲</label>
                                        </div>
                                        <div class="col-9 form-inline">
                                            <?php
                                            // gen_person_color_checkbox_html('8', $person_color_index);
                                            gen_person_color_select_html('8', $person_color_index);
                                            ?>
                                            <!-- <label for="person_item_color">衣著顏色：</label>
                                            <select id="person_item_color" name="person_item_color" class="custom-select">
                                                <option value="type81" selected>黑色</option>
                                                <option value="type82" >藍色</option>
                                                <option value="type83">咖啡色</option>
                                                <option value="type84">灰色</option>
                                                <option value="type85">綠色</option>
                                                <option value="type86">橘色</option>
                                                <option value="type87">粉紅色</option>
                                                <option value="type88">紫色</option>
                                                <option value="type89">紅色</option>
                                                <option value="type810">白色</option>
                                                <option value="type811">黃色</option>
                                            </select> -->
                                            <!-- &emsp;&emsp;&emsp;
                                            <label for="person_item_class">衣著型式：</label>
                                            <select id="person_item_class" name="person_item_class" class="custom-select">
                                                <option value="">短袖</option>
                                                <option value="">長袖</option>
                                                <option value="">短褲</option>
                                                <option value="">長褲</option>
                                            </select> -->

                                            <!-- <label for='type61'>短袖上衣</label>
                                            <input name='type61' id='type61' type='checkbox' value='short_sleeves_top'>
                                            <label for='type62'>長袖上衣</label>
                                            <input name='type62' id='type62' type='checkbox' value='long_sleeves_top'>
                                            <label for='type63'>短褲&emsp;</label>
                                            <input name='type63' id='type63' type='checkbox' value='shorts'>
                                            <label for='type64'>長褲&emsp;</label>
                                            <input name='type64' id='type64' type='checkbox' value='trousers'> -->
                                        </div>



                                        <div class="col-3">
                                            <input name="type9" id="type9" type="checkbox" value="trousers"><label for="type9">行人：長褲</label>
                                        </div>
                                        <div class="col-9 form-inline">
                                            <?php
                                            // gen_person_color_checkbox_html('9', $person_color_index);
                                            gen_person_color_select_html('9', $person_color_index);
                                            ?>
                                            <!-- <label for="person_item_color">衣著顏色：</label>
                                            <select id="person_item_color" name="person_item_color" class="custom-select">
                                                <option value="type91" selected>黑色</option>
                                                <option value="type92" >藍色</option>
                                                <option value="type93">咖啡色</option>
                                                <option value="type94">灰色</option>
                                                <option value="type95">綠色</option>
                                                <option value="type96">橘色</option>
                                                <option value="type97">粉紅色</option>
                                                <option value="type98">紫色</option>
                                                <option value="type99">紅色</option>
                                                <option value="type910">白色</option>
                                                <option value="type911">黃色</option>
                                            </select> -->
                                            <!-- &emsp;&emsp;&emsp;
                                            <label for="person_item_class">衣著型式：</label>
                                            <select id="person_item_class" name="person_item_class" class="custom-select">
                                                <option value="">短袖</option>
                                                <option value="">長袖</option>
                                                <option value="">短褲</option>
                                                <option value="">長褲</option>
                                            </select> -->

                                            <!-- <label for='type61'>短袖上衣</label>
                                            <input name='type61' id='type61' type='checkbox' value='short_sleeves_top'>
                                            <label for='type62'>長袖上衣</label>
                                            <input name='type62' id='type62' type='checkbox' value='long_sleeves_top'>
                                            <label for='type63'>短褲&emsp;</label>
                                            <input name='type63' id='type63' type='checkbox' value='shorts'>
                                            <label for='type64'>長褲&emsp;</label>
                                            <input name='type64' id='type64' type='checkbox' value='trousers'> -->
                                        </div>



                                    </div>
                                </div>
                            </form>
                            <button id="btn_query" class="btn btn-success">檢索</button><br>
                        </div>

                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">檢索結果
                            <div id="query_result">
                            </div>

                        </div>

                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...contact</div>
                    </div>
                </div>
            </div>
            
            <!-- <div class="row">
                <div class="col-4 border">
                    
                </div>
                <div class="col-8 border">
                    主要內容
                    
                </div>
            </div> -->
        </div>
    </div>




    <!-- <div class="container">
    <div class="row">
        <div class='col-sm-6'>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>
    </div>
</div> -->



<!-- <div class="container">
    <div class="row">
        <div class="col-sm-6">
            <input type="text" class="form-control datetimepicker-input" id="datetimepicker5" data-toggle="datetimepicker" data-target="#datetimepicker5"/>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker5').datetimepicker({
                    locale: 'zh-tw',//選擇語系
                    icons: {
                        time: "far fa-clock",
                        date: 'fas fa-calendar',
                        up: 'fas fa-arrow-up',
                        down: 'fas fa-arrow-down',
                        previous: 'fas fa-chevron-left',
                        next: 'fas fa-chevron-right',
                        today: 'fas fa-calendar-check-o',
                        clear: 'fas fa-trash',
                        close: 'fas fa-times'
                    }//更換icon

                });
            });
        </script>
    </div>
</div> -->

    <!-- 檢索按鍵 -->
    <script type="text/javascript">
        $("#btn_query").click(function(){
            var data = $('form').serializeArray();
            $.get("get_police_data.php", data, function(result){
                alert("檢索完成");
                // alert(result);
                $("#query_result").html(result);
                $("#nav-tab li:nth-child(2) a").tab('show');
            }, );
            
            // $.get( "get_police_data.php", function( data ) {
            //     alert( "Data Loaded: " + data );
            // });

            // $.ajax({
            //     url: "get_police_data.php",
            //     type: 'GET',
            //     data: data,
            //     success: function(result){
            //     alert("分析完成");
            //     // alert(result);
            //     $("#query_result").html(result);
            //     // $("#nav-home-tab").removeClass('active');
            //     $("#nav-tab li:nth-child(2) a").tab('show');
            //     // $("#myTab   li:first-child a").tab('show');
            //     },
            //     // dataType: dataType
            // });


        });

    </script>

    <!-- Optional JavaScript -->
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker5').datetimepicker({
                locale: 'zh-tw',//選擇語系
                format: 'YYYY-M-D HH:mm',
                icons: {
                    time: "far fa-clock",
                    date: 'fas fa-calendar',
                    up: 'fas fa-arrow-up',
                    down: 'fas fa-arrow-down',
                    previous: 'fas fa-chevron-left',
                    next: 'fas fa-chevron-right',
                    today: 'fas fa-calendar-check-o',
                    clear: 'fas fa-trash',
                    close: 'fas fa-times'
                }//更換icon

            });
        });

        $(function () {
            $('#datetimepicker_end').datetimepicker({
                locale: 'zh-tw',//選擇語系
                format: 'YYYY-M-D HH:mm',
                icons: {
                    time: "far fa-clock",
                    date: 'fas fa-calendar',
                    up: 'fas fa-arrow-up',
                    down: 'fas fa-arrow-down',
                    previous: 'fas fa-chevron-left',
                    next: 'fas fa-chevron-right',
                    today: 'fas fa-calendar-check-o',
                    clear: 'fas fa-trash',
                    close: 'fas fa-times'
                }//更換icon

            });
        });
    </script>

    <!-- upload files -->
    <script type="text/javascript">
        $('#btn_upload').on('click', function() {
            // 上傳開始後關閉分析按鍵
            $('#btn_query').attr('disabled', true);
            var file_data = $('#customFile').prop('files')[0];
            var fps = $('#fps').val();
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('fps', fps);
            // alert(form_data);                             
            $.ajax({
                url: 'upload.php', // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(response_json){
                    var message = JSON.parse(response_json);
                    alert(message.log); // display response from the PHP script, if any
                    $("#upload_file_list").text(message.upload_file_name);
                    // 上傳完畢後關閉分析按鍵
                    $('#btn_query').attr('disabled', false);
                }
            });
        });
    </script>

    <!-- validate 車輛 checkbox -->
    <script type="text/javascript"></script>





</body>
</html>
